import { Component, EventEmitter, Output } from '@angular/core';

@Component({
selector: 'app-alumno-input',
templateUrl: 'alumno-input.component.html',
styleUrls: ['alumno-input.component.css']


})

export class AlumnoInputComponent{
  @Output() addAlumno = new EventEmitter<string>();
  nombre: string = '';

  alta(){

    this.addAlumno.emit(this.nombre);
    this.nombre = '';
  }
}






