import{ Component, Input }from '@angular/core';

@Component({
  selector: 'app-alumno',
  templateUrl: './alumno.component.html',
  styleUrls: ['./alumno.component.css']
})

export class AlumnoComponent{
activo: boolean = true;
//alumnos: string[] = ['Rogelio','Jorge', 'Ana'];


@Input() alumnos: string[] = ['Rogelio','Jorge', 'Ana'];


OnClickActivar(){

  this.activo = !this.activo;
}
}

